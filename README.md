# Blood Oxygen And Heartbeat Monitoring (PulsOxi) Documentation

The shortname of this project is called **PulsOxi**.

Since Covid 19 existed, spontaneous outbreaks have been recorded time by time, which often hit the health systems unexpectedly. 
This project is intended to create an effective, simple and cheap tooling which makes the monitoring of patients pulse / blood oxygen level in the event of a disaster very quickly and easily. 

The rescue workers can than essentially take care of the patient's and only a few people are needed for monitoring the pulse / blood oxygen level of the patients.

The next picture shows an overview of the used hardware and open source software components.
![](docs/overview.png)
PulsOxi
